@extends('main.main')

@section('main')


<div class="container body">
    <div class="read-body">

        <div class="read-top">
            <div class="title-read">
                <h2>{{ $chapter->name }}</h1>
            </div>

            <div class="img-read">
                <img src="{{ asset('assets/uploads/chapter/'.$chapter->image) }}" alt="Cover">
            </div>

            <div class="link-read">
                @if ($previous)
                    <a href="/read/{{ $novel->slug }}/{{ $previous->slug }}"><i class="bi bi-skip-backward me-1"></i> Sebelumnya</a>
                @else
                    <span href="#"><i class="bi bi-skip-backward me-1"></i> Sebelumnya</span>
                @endif
                |
                <a href="{{ url('/detail/'.$novel->slug) }}"><i class="bi bi-list-ul"></i> Daftar Isi</a>
                |
                @if ($next)
                    <a href="/read/{{ $novel->slug }}/{{ $next->slug }}">Selanjutnya <i class="bi bi-skip-forward ms-1"></i></a>
                @else
                    <span href="#">Selanjutnya <i class="bi bi-skip-forward ms-1"></i></span>
                @endif
            </div>
        </div>

        <div class="read-mid">
            <div class="text">
                {!! $chapter->text !!}
            </div>
        </div>

        <div class="read-bottom">
            <div class="link-read">
                @if ($previous)
                    <a href="/read/{{ $novel->slug }}/{{ $previous->slug }}"><i class="bi bi-skip-backward me-1"></i> Sebelumnya</a>
                @else
                    <span href="#"><i class="bi bi-skip-backward me-1"></i> Sebelumnya</span>
                @endif
                |
                <a href="{{ url('/detail/'.$novel->slug) }}"><i class="bi bi-list-ul"></i> Daftar Isi</a>
                |
                @if ($next)
                    <a href="/read/{{ $novel->slug }}/{{ $next->slug }}">Selanjutnya <i class="bi bi-skip-forward ms-1"></i></a>
                @else
                    <span href="#">Selanjutnya <i class="bi bi-skip-forward ms-1"></i></span>
                @endif
            </div>
        </div>
    </div>
</div>


@endsection
